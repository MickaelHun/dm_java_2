import java.util.*;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer mini;
        if (liste.size() == 0)
            mini = null;
        else{
            mini = liste.get(0);
            for (Integer nombre : liste)
                if (nombre < mini)
                    mini = nombre;
        }
        return mini;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        boolean res;
        if (liste.size() == 0)
            res = true;
        else{
            T mini = Collections.min(liste);
            res = false;
            if (valeur.compareTo(mini) < 0)
                res = true;
        }
        return res;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> liste3 = new ArrayList<>();
        if (liste1.size() != 0 && liste2.size() != 0){
            if (Collections.min(liste1).compareTo(Collections.max(liste2)) <= 0 && Collections.min(liste2).compareTo(Collections.max(liste1)) <= 0){
                for (T elem : liste1)
                    if (!liste3.contains(elem) && liste2.contains(elem)){
                        liste3.add(elem);
                    }
            }
        }
        return liste3;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> listeMots = new ArrayList<>();
        if (texte != ""){
            String mot = "";
            for (int i = 0; i<texte.length(); ++i){
                char c = texte.charAt(i);
                if (c == ' '){
                    if (mot != ""){
                        listeMots.add(mot);
                        mot = "";
                    }
                }
                else{
                    mot = mot+c;
                }
            }
            if (mot != ""){
                listeMots.add(mot);
                mot = "";
            }
        }
        return listeMots;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        String motMax = null;
        if (texte.length() != 0){
            List<String> listeMots = BibDM.decoupe(texte);
            Map<String, Integer> dicoFreq = new HashMap<>();
            for (String mot : listeMots){
                if (dicoFreq.containsKey(mot))
                    dicoFreq.put(mot, dicoFreq.get(mot)+1);
                else
                    dicoFreq.put(mot, 1);
            }
            Integer freqMax = Collections.max(dicoFreq.values());
                for (String mot : dicoFreq.keySet()){
                    Integer freq = dicoFreq.get(mot);
                    if (freq == freqMax)
                        if (motMax == null) {motMax = mot;}
                        else {if (mot.compareTo(motMax) < 0) {motMax = mot; }}    
            }
        }
        return motMax;

    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int cpt = 0;
        for (int i = 0; i<chaine.length(); ++i){
            if (chaine.charAt(i) == '(') {cpt += 1;}
            else if (chaine.charAt(i) == ')') {
                if (cpt == 0) {return false;}
                else {cpt -= 1;}
            }
        }
        return cpt == 0;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        boolean crochetsActifs = false;
        int cptC = 0;
        int cptP = 0;
        for (int i = 0; i<chaine.length(); ++i){
            if (crochetsActifs){
                if (chaine.charAt(i) == '[') {cptC += 1;}
                else if (chaine.charAt(i) == ']') {
                    if (cptC == 0) {return false;}
                    else {
                        cptC -= 1;
                        if (cptC == 0) {crochetsActifs = false;}
                    }
                }
            }
            else{
                if (chaine.charAt(i) == '[') {
                    crochetsActifs = true;
                    cptC += 1;
                }
                else if (chaine.charAt(i) == ']') {return false;}
                else{
                    if (chaine.charAt(i) == '(') {cptP += 1;}
                    else if (chaine.charAt(i) == ')') {
                        if (cptP == 0) {return false;}
                        else {cptP -= 1;}
                    }
                }
            }
        }
        return cptP == 0 && cptC == 0;
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        int mediane;
        int fin = liste.size();
        int debut = 0;
        boolean ok = false;
        while (debut < fin && !ok){
            mediane = ((debut + fin) / 2);

            if (valeur.compareTo(liste.get(mediane)) < 0){
                fin = mediane;
            }

            else if (valeur.compareTo(liste.get(mediane)) > 0){
                debut = mediane + 1;
            }
            
            else{
                ok = true;
            }
        }
        return ok;

    }




}
